﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventExample
{
    public delegate void MojEventHandler(object sender, MojEventArgs e);

    public class MojEventArgs: EventArgs
    {
        private string EventInfo;
        public MojEventArgs(string EventInfo)
        {
            this.EventInfo = EventInfo;
        }
        public string GetInfo()
        {
            return EventInfo;
        }
    }
    public class Klasa
    {
        public event MojEventHandler onMaxiumum;
        private int i;
        private int Maxiumum = 10;
        public int MyValue
        {
            get
            {
                return i;
            }
            set
            {
                if(value <= Maxiumum)
                {
                    i = value;
                }
                else
                {
                    if(onMaxiumum != null)
                    {
                        onMaxiumum(this, new MojEventArgs("Gitara siema" + value.ToString()+"max to: "+Maxiumum.ToString()));
                    }
                }
            }
        }
    }
    class Program
    {
        static void MaximumReached(object sender, MojEventArgs e)
        {
            Console.WriteLine(e.GetInfo());
        }
        static void Main(string[] args)
        {
            Klasa k = new Klasa();
            k.onMaxiumum += new MojEventHandler(MaximumReached);

            for(int n = 0; n < 15; n++)
            {
                k.MyValue = n;
            }

            Console.ReadKey();
        }
    }
}
